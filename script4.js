"use strict"

let urlFilms = "https://ajax.test-danit.com/api/swapi/films";
let getContainer = document.querySelector(".conteiner");
let loader = document.querySelector(".loader");


fetch(urlFilms)
  .then((res) => res.json())
  .then((filmsData) => {
    // console.log(filmsData);
    filmsData.forEach(({id, episodeId, name, openingCrawl }) => {
    getContainer.insertAdjacentHTML("beforeend", `
      <div class="conteinerCard">
        <h1>Episode :  ${episodeId}</h1>
        <h2>${name}</h2>
        <p>${openingCrawl}</p>
      </div>
    `);
  
    });
   
    let charPromises = filmsData.map(({ characters }) => {
      return Promise.all(characters.map((charUrl) => fetch(charUrl).then((res) => res.json())));
    });

    Promise.all(charPromises)
      .then((charactersDataArray) => {
        filmsData.forEach(({id}, index) => {
          let charactersData = charactersDataArray[index];
          let charNames = charactersData.map(({ name }) => name).join(", ");
          let cardContainer = getContainer.querySelector(`.conteinerCard:nth-child(${(id)})`);
          if (cardContainer) {
            let charactersList = document.createElement("div");
            charactersList.textContent = `ACTORS LIST:   ${charNames}`;
            cardContainer.append(charactersList);
          }
         
        });
        loader.remove()
     
      })
      .catch((error) => {
        console.error("Error character data:", error);
      });
  })
  .catch((error) => {
    console.error("Error film data:", error);
  });

 















       

       
